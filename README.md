# Descriptives
Création de tableaux et graphiques à partir d'une ou deux variables.

## Utilisation

**Installation du package :**
```{r}
if (!require("devtools")) install.packages("devtools", dep=T)
devtools::install_git("https://framagit.org/Grisoudre/FramaDescri")
```

**Ouverture de l'application :**
```{r}
library(Descriptives)
Descri()
```
Puis, sur la nouvelle fenêtre, cliquer sur "Open in browser".
